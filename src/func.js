const getSum = (str1, str2) => {
  let sum = Number(str1) + Number(str2);
  return (isNaN(sum) || sum == 0) ? false : sum.toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = listOfPosts.filter(x => x.author == authorName).length;
  let comments = 0;
  for (let post of listOfPosts){
    if ('comments' in post){
      for (let comment of post.comments){
        if(comment.author == authorName){
          comments++;
        }
      }
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets= (people) => {
  let bills = [];
  people = people.map(x => Number(x));
  for(let element of people) {
    switch(element){

      case 100:
        if (bills.includes(50) && bills.includes(25)){
          bills.splice(bills.indexOf(50), 1);
          bills.splice(bills.indexOf(25), 1);
        } else if (bills.filter(x => x == 25).length >= 3){
          for(let i = 0; i < 3; i++){
            bills.splice(bills.indexOf(25), 1);
          }
        } else{
          return 'NO';
        }
        break;

      case 50:
        if (bills.includes(25)){
          bills.splice(bills.indexOf(25), 1);
        } else{
          return 'NO';
        }
        break;

      case 25:
        break;
    }
    bills.push(element);
  }
  return 'YES';
};



module.exports = {getSum, getQuantityPostsByAuthor, tickets};
